# This is a sample Python script.

# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.
from sklearn.datasets import load_iris
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn2pmml import sklearn2pmml
from sklearn2pmml.pipeline import PMMLPipeline
#import lightgbm

import joblib
import pandas as pd
import numpy as np
import json
def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press ⌘F8 to toggle the breakpoint.



# Press the green button in the gutter to run the script.
if __name__ == '__main__':


    iris = load_iris()
    colors = ["blue", "red", "green"]
    df = pd.DataFrame(
        data=np.c_[iris["data"], iris["target"]], columns=iris["feature_names"] + ["target"]
    )

    Xtrain, XTest, Ytrain, YTest = train_test_split(iris.data, iris.target, test_size=0.3, random_state=4)
    model = LogisticRegression(C=0.1, max_iter=20, fit_intercept=True, n_jobs=3, solver='liblinear')
    model.fit(Xtrain, Ytrain)
    print(model.coef_)

    pipeline = PMMLPipeline([("classifier", model)])
    pipeline.fit(Xtrain, Ytrain)
    print(pipeline.score)
    sklearn2pmml(pipeline, 'demo.pmml', with_repr=True)


    joblib.dump(model, "joblib_model.pkl")
    joblib_model = joblib.load("joblib_model.pkl")

    score = joblib_model.score(XTest, YTest)
    #print(score)

    #model_param = {}
    #model_param['coef'] = list(model.coef_)
    #model_param['intercept'] = model.intercept_.tolist()
    #json_txt = json.dumps(model_param, indent=4)
    #with open('regression_param.txt', 'w') as file:
    #    file.write(json_txt)
    #with open('regression_param.txt', 'r') as file:
    #   dict_ = json.load(file)

    print_hi('PyCharm')


# See PyCharm help at https://www.jetbrains.com/help/pycharm/
